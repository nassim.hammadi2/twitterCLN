package macron.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="tweet")
public class Tweet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String autor;
	private String date;
	private String content;
	
	public Tweet(int id, String autor, String date, String content) {
		super();
		this.id = id;
		this.autor = autor;
		this.date = date;
		this.content = content;
	}
	
	public Tweet(String autor, String date, String content) {
		
		this.autor = autor;
		this.date = date;
		this.content = content;
	}
	
	
	public Tweet() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Twitter [id=" + id + ", autor=" + autor + ", date=" + date + ", content=" + content + "]";
	}
	
	
}
