package macron.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import macron.domain.Tweet;
import macron.repository.TwitterRepository;

@Service
@Transactional
public class TwitterService {

	 
	  private TwitterRepository tRepo;

	  public TwitterService() {
	  }

	  @Autowired
	  public void setTwitterService(TwitterRepository tRepo) {
	      this.tRepo = tRepo;
	  }


	  public Tweet insert(Tweet tweet) {
	      return tRepo.save(tweet);
	  }
}
