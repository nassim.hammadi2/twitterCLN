package macron.repository;

import org.springframework.stereotype.Repository;

import macron.domain.Tweet;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterRepository extends CrudRepository<Tweet, Integer> {

}
